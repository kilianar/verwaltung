# Mathezirkel Verwaltung

![License: GPL-3.0](https://img.shields.io/badge/License-GPL--3.0-green)

This project contains some scripts for the organization of the Mathezirkel Augsburg.

## Authors and acknowledgment
Most work was done by Kathrin Helmsauer, Sven Prüfer, Anna Rubeck and Felix Stärk.

## License
This project is licenced unter GPL-3.0, see [LICENSE](./LICENSE).
