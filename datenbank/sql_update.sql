-- verwaltung
-- Copyright (C) 2022  Mathezirkel Augsburg

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

UPDATE
	"Teilnehmer_gesamt"
SET
	"Teilnehmer_gesamt"."Vorname" =
			( SELECT "neue_Daten"."Vorname"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Nachname" =
			( SELECT "neue_Daten"."Nachname"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Geschlecht" =
			( SELECT "neue_Daten"."Geschlecht"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Straße" =
			( SELECT "neue_Daten"."Straße"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."PLZ" =
			( SELECT "neue_Daten"."PLZ"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Ort" =
			( SELECT "neue_Daten"."Ort"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."E-Mail Eltern" =
			( SELECT "neue_Daten"."E-Mail Eltern"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."E-Mail Schüler" =
			( SELECT "neue_Daten"."E-Mail Schüler"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Festnetz" =
			( SELECT "neue_Daten"."Festnetz"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Handy Schüler" =
			( SELECT "neue_Daten"."Handy Schüler"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Geburtstag" =
			( SELECT "neue_Daten"."Geburtstag"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Schule" =
			( SELECT "neue_Daten"."Schule"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Notfallname 1" =
			( SELECT "neue_Daten"."Notfallname 1"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Notfallnummer 1" =
			( SELECT "neue_Daten"."Notfallnummer 1"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Notfallname 2" =
			( SELECT "neue_Daten"."Notfallname 2"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Notfallnummer 2" =
			( SELECT "neue_Daten"."Notfallnummer 2"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Notfallname 3" =
			( SELECT "neue_Daten"."Notfallname 3"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Notfallnummer 3" =
			( SELECT "neue_Daten"."Notfallnummer 3"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Geheime Bemerkungen" =
			( SELECT "neue_Daten"."Geheime Bemerkungen"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			),
	"Teilnehmer_gesamt"."Wettbewerbe" =
			( SELECT "neue_Daten"."Wettbewerbe"
			  FROM "neue_Daten"
			  WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			)
WHERE EXISTS
			( SELECT "neue_Daten"."Vorname"
			FROM "neue_Daten"
			WHERE "Teilnehmer_gesamt"."SchülerID" = "neue_Daten"."SchülerID"
			)