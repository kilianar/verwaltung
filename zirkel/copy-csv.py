#!/usr/bin/python
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from shutil import copy2
from pathlib import Path
from argparse import ArgumentParser

# Lese Argumente
parser = ArgumentParser()
# Tabelle mit den Daten. Die erste Spalte muss die ZirkelID enthalten.
parser.add_argument("-s", "--source", dest="CSV_SOURCE", default="./csv/", required=False)
# Ordner, in den die erstellten Dateien gespeichert werden sollen.
parser.add_argument("-d", "--dest", dest="ZIRKEL_DIR", default="../../mathezirkel/2223/", required=False)

args = parser.parse_args()

# Konvertiere in Pfadobject
CSV_SOURCE = Path(args.CSV_SOURCE)
ZIRKEL_DIR = Path(args.ZIRKEL_DIR)

DATA_DIR_NAMES = ("daten", "Daten", "adressen", "Adressen")

def getDataDirName(zirkel_dir : Path, data_dir_names) -> Path:
    for name in data_dir_names:
        zirkel_data_dir = zirkel_dir / name
        if zirkel_data_dir.is_dir():
            return zirkel_data_dir
    raise ValueError(f"There is no data directory: {zirkel_dir}")

files = list(CSV_SOURCE.glob("*.csv")) + list(CSV_SOURCE.glob("*.txt"))

for datei in files:
    if "P" in datei.name:
        zirkel = datei.name.split(".")[0].split('-')[0]
        zirkel_data_dir = getDataDirName(ZIRKEL_DIR / "praesenzzirkel" / zirkel, DATA_DIR_NAMES)
        copy2(datei, zirkel_data_dir)
    elif "K" in datei.name:
        zirkel = datei.name.split(".")[0].split('-')[0]
        zirkel_data_dir = getDataDirName(ZIRKEL_DIR / "korrespondenzzirkel" / zirkel, DATA_DIR_NAMES)
        copy2(datei, zirkel_data_dir)
