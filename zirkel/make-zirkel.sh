#!/bin/sh
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

YEAR="2223"
TAG="1.4"

MC_CONFIG_FILE="$MATHEZIRKEL_DIR/verwaltung/$YEAR/zirkel/listen/config.yaml"
MC_LIST_DIR="$MATHEZIRKEL_DIR/verwaltung/$YEAR/zirkel/listen/"
ZIRKEL_DIR="$MATHEZIRKEL_DIR/mathezirkel/$YEAR/"

echo $MC_LIST_DIR

# Create files
docker run \
    -v "$MC_LIST_DIR":/app/lists \
    -v "$MC_CONFIG_FILE":/app/config.yaml \
    registry.gitlab.com/mathezirkel/management/datenbank-exporte:"$TAG"

# Copy files to the corresponding directories
python3 ./copy-csv.py -s "$MC_LIST_DIR/csv/" -d "$ZIRKEL_DIR"
