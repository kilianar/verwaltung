#!/bin/bash
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# MATHEZIRKEL_DIR="${HOME}/Mathezirkel-Cloud"
DOCUMENT_SRC="${HOME}/Projects/mathezirkel/management/dokumente"

set -e

MC_EVENT="mathecamp"
MC_YEAR="2223"

MC_CONFIG_FILE="${MATHEZIRKEL_DIR}/verwaltung/${MC_YEAR}/${MC_EVENT}/config/config.yaml"
MC_CONFIG_DIR="${MATHEZIRKEL_DIR}/verwaltung/${MC_YEAR}/${MC_EVENT}/config"
MC_LIST_DIR="${MATHEZIRKEL_DIR}/verwaltung/${MC_YEAR}/${MC_EVENT}/listen"
MC_DOCUMENT_DIR="${MATHEZIRKEL_DIR}/verwaltung/${MC_YEAR}/${MC_EVENT}/dokumente"
MC_IMAGE_DIR="${MATHEZIRKEL_DIR}/verwaltung/share/images"
MC_RAUMPLAN_DIR="${MATHEZIRKEL_DIR}/verwaltung/share/mathecamp/raumplan"

docker run \
    -v $MC_LIST_DIR:/app/lists \
    -v $MC_CONFIG_FILE:/app/config.yaml \
    registry.gitlab.com/mathezirkel/management/datenbank-exporte:1.4

docker run \
    -v $MC_LIST_DIR:/app/lists \
    -v $MC_DOCUMENT_DIR:/app/documents \
    -v $MC_CONFIG_FILE:/app/config.yaml \
    registry.gitlab.com/mathezirkel/management/mathecamp-zirkelplan-exporte:1.2

cd $DOCUMENT_SRC
source run.sh

